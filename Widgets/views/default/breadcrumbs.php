<?php
/**
 * @var $breadcrumbs array
 */
?>
<div class="breadcrumbs-widget">
    <ol class="breadcrumb">
        <?php foreach($breadcrumbs as $name => $link) : ?>
        <li class="breadcrumb-item"><a href="<?=$link?>"><?=$name?></a></li>
        <?php endforeach; ?>
    </ol>
</div>