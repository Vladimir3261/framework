<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 4/29/17
 * Time: 5:49 PM
 */
namespace Controller\Admin;

use Component\AuthComponent;
use Controller\BaseControllers\AdminController;
use isv\IS;
use isv\View\ViewBase;
use Models\BrandsModel;
use Models\JsonSettingsModel;
use Models\OffersModel;
use Models\UsersModel;

/**
 * Class CategoriesController
 * @package Controller\Admin
 */
class SettingsController extends AdminController
{
    public function init()
    {
        parent::init();
        IS::app()->breadcrumbs()->add('Admin', '/admin');
        IS::app()->breadcrumbs()->add('Brands', '/admin/brands');
    }

    public function indexAction()
    {
        if(IS::app()->request()->isPost()) {
            $obj = IS::app()->settings('frontend');
            $settings = [];
            foreach ($obj as $k=>$v){
                $settings[$k] = (array)$v;
            }
            $data = IS::app()->request()->postData('settings');
            foreach ($settings as $k => $setting){
                if(isset($data[$k])){
                    if($settings[$k]['input'] === 'image')
                        $data[$k] = str_replace('http://'.$_SERVER['HTTP_HOST'], '', $data[$k]);
                    $settings[$k]['value'] = $data[$k];
                }
            }
            file_put_contents(ROOTDIR.DIRSEP.'config'.DIRSEP.'json'.DIRSEP.'frontend.json', json_encode($settings));
            $this->redirect(IS::app()->request()->refer());
        }
        return new ViewBase([
            'frontend' => IS::app()->settings('frontend'),
            'offer' => new OffersModel(['offerType' => OffersModel::CHECKOUT])
        ]);
    }

    public function offerAction()
    {
        $offer = new OffersModel(['offerType' => OffersModel::CHECKOUT]);
        if($offer->load(IS::app()->request()->postData()) && $offer->save())
            IS::app()->session()->setFlash('success', 'Offer text saved');
        else
            IS::app()->session()->setFlash('error', 'Error. Service temporary unavailable');
        $this->redirect(IS::app()->request()->refer());
    }

    public function passwordAction()
    {
        $current = IS::app()->request()->postData('current');
        $newPassword = IS::app()->request()->postData('password');
        $match = IS::app()->request()->postData('password_match');
        if($newPassword !== $match) {
            IS::app()->session()->setFlash('error', 'Passwords doesn\'t match');
        } else {
            /**
             * @var $component AuthComponent
             */
            $component = IS::app()->component('auth');
            if($component->changePassword($current, $newPassword))
                IS::app()->session()->setFlash('success', 'Password success changed');
            else
                IS::app()->session()->setFlash('error', 'Incorrect password. Please try again');
        }
        $this->redirect(IS::app()->request()->refer());
    }
}