<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 5/3/17
 * Time: 3:41 AM
 */
namespace Controller\Admin;
use Component\AuthComponent;
use Controller\BaseControllers\AdminController;
use isv\IS;
use isv\View\ViewBase;
use Models\SignUpModel;

class IndexController extends AdminController
{
    /**
     * @var $auth AuthComponent
     */
    private $auth;

    public function init()
    {
        $this->auth = IS::app()->component('auth');
        parent::init();
    }

    public function indexAction()
    {
       return new ViewBase();
    }

    public function loginAction()
    {
        IS::app()->set('layout', 'login', true);
        if($this->auth->isAuth())
            $this->redirect('/admin');
        $error = false;
        if(IS::app()->request()->isPost())
        {
            $login = IS::app()->request()->postData('username');
            $password = IS::app()->request()->postData('password');
            if($login && $password)
            {
                $model = new SignUpModel(0);
                $result = $model->login($login, $password, IS::app()->request()->postData('remember'));
                if($result === SignUpModel::INVALID_PASSWORD || $result === SignUpModel::USER_NOT_EXISTS)
                    $error = ['class' => 'alert-danger', 'message' => 'Login or password incorrect'];
                elseif ($result === SignUpModel::VERIFICATION_REQUIRED)
                    $error = ['class' => 'alert-info', 'message' => 'Please, confirm you email address'];
                elseif ($result === SignUpModel::USER_BLOCKED)
                    $error = ['class' => 'alert-warning', 'message' => 'You account is blocked'];
                elseif($result === true) {
                    if(IS::app()->user()->id == IS::app()->getConfig('config')['admin'])
                        $this->redirect('/admin');
                    else
                        $this->redirect('/user');
                }
                else
                    $this->redirect(IS::app()->request()->refer());
            }
            else
            {
                $error = ['class' => 'alert-warning', 'message' => 'Invalid login and password format'];
            }
        }
        return new ViewBase([
            'error' => $error,
        ]);
    }

    public function logoutAction()
    {
        $this->auth->logout();
        $this->redirect('/');
    }
}