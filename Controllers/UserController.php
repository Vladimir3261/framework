<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 4/25/17
 * Time: 11:23 AM
 */
namespace Controller;

use Component\AuthComponent;
use isv\Controller\ControllerBase;
use isv\Helper\Random;
use isv\IS;
use isv\Notifications\Mailer;
use isv\View\ViewBase;
use Models\SignUpModel;
use Models\UserDataModel;
use Models\UsersModel;
class UserController extends ControllerBase
{
    /**
     * @var $auth AuthComponent
     */
    private $auth;

    public function init()
    {
        IS::app()->set('title', 'Account');
        $this->auth = IS::app()->component('auth');
        $action = IS::app()->get('action');
        if(!$this->auth->isAuth() && $action !== 'loginAction' && $action !== 'registerAction' && $action !== 'confirmAction' && $action !== 'resetAction'){
            $this->redirect('/user/login');exit(1);
        }
    }

    public function indexAction()
    {
        if(IS::app()->request()->isPost())
        {
            $data = IS::app()->request()->postData('data');
            foreach (IS::app()->settings('auth')->fields as $fieldName => $fieldSettings) {
                if(isset($data[$fieldName]) && $data[$fieldName]) {
                    $dataModel = new UserDataModel(['label' => $fieldName, 'userId' => IS::app()->user()->id]);
                    $dataModel->setLabel($fieldName)
                        ->setUserId(IS::app()->user()->id)
                        ->setVal($data[$fieldName])->save();
                    unset($dataModel);
                }
            }
            $mainData = IS::app()->request()->postData('user');
            if($mainData)
            {
                if(isset($mainData['email']) && $mainData['email'] && filter_var($mainData['email'], FILTER_VALIDATE_EMAIL))
                {
                    $user = new UsersModel(['email' => $mainData['email']]);
                    if( !$user->insert && (int)$user->getId() !== (int)IS::app()->user()->id ) {
                        IS::app()->session()->setFlash('user_error', 'User '.$user->getEmail().' already has registered');
                    }
                    else {
                        // Update user email
                        $currentUserModel = new UsersModel((int)IS::app()->user()->id);
                        $currentUserModel->setEmail($mainData['email'])->save();
                        IS::app()->session()->setFlash('user_success', 'Data success updated');
                    }
                }
            }
            $this->redirect('/user');
        }
        $model = new UsersModel((int)IS::app()->user()->id);
        return new ViewBase([
            'model' => $model,
        ]);
    }

    public function loginAction()
    {
        if($this->auth->isAuth())
            $this->redirect('/user');
        $error = false;
        if(IS::app()->request()->isPost())
        {
            $login = IS::app()->request()->postData('username');
            $password = IS::app()->request()->postData('password');
            if($login && $password)
            {
                $model = new SignUpModel(0);
                $result = $model->login($login, $password, IS::app()->request()->postData('remember'));
                if($result === SignUpModel::INVALID_PASSWORD || $result === SignUpModel::USER_NOT_EXISTS)
                    $error = ['class' => 'alert-danger', 'message' => 'Login or password incorrect'];
                elseif ($result === SignUpModel::VERIFICATION_REQUIRED)
                    $error = ['class' => 'alert-info', 'message' => 'Please, confirm you email address'];
                elseif ($result === SignUpModel::USER_BLOCKED)
                    $error = ['class' => 'alert-warning', 'message' => 'You account is blocked'];
                elseif($result === true) {
                    if(IS::app()->user()->id == IS::app()->getConfig('config')['admin'])
                        $this->redirect('/admin');
                    else
                        $this->redirect('/user');
                }
                else
                    $this->redirect(IS::app()->request()->refer());
            }
            else
            {
                $error = ['class' => 'alert-warning', 'message' => 'Invalid login and password format'];
            }
        }
        return new ViewBase([
            'error' => $error,
        ]);
    }

    public function registerAction()
    {
        if($this->auth->isAuth())
            $this->redirect('/user');
        $model = new SignUpModel(0);
        if(IS::app()->request()->isPost())
        {
            if( $model->load(IS::app()->request()->postData()) && $model->save()) {
                Mailer::sendMessage($model->getEmail(), 'Verify you account', Mailer::prepare('register', [
                    '{USERNAME}' => IS::app()->request()->postData('firstName'),
                    '{EMAIL}' => IS::app()->request()->postData('email'),
                    '{SITE}' => $_SERVER['HTTP_HOST'],
                    '{VERIFY_LINK}' => 'http://'.$_SERVER['HTTP_HOST'].'/user/confirm?token='.$model->getRememberToken(),
                ]));
                IS::app()->session()->setFlash('success', 'Register success. Please confirm your email:  '.$model->getEmail());
                $this->redirect('/');
            }
            else {
                $model->setPassword(NULL)->setPasswordMatch(NULL);
            }

        }
        return new ViewBase([
            'model' => $model,
            'form' => $model->getFrom(),
        ]);
    }

    /**
     * Confirm user email address
     * @return void
     */
    public function confirmAction()
    {
        $token = $_GET['token'];
        $model = new UsersModel(['rememberToken' => $token]);
        if($model && !$model->insert && !$model->getConfirmed()) {
            $model->setConfirmed(SignUpModel::CONFIRMED_EMAIL)->save();
            IS::app()->session()->setFlash('success', 'Email success confirmed');
        }
        else
        {
            IS::app()->session()->setFlash('error', 'Link expired');
        }
        $this->redirect('/');
    }

    public function logoutAction()
    {
        $this->auth->logout();
        $this->redirect('/');
    }

    public function resetAction()
    {
        if(IS::app()->user()->isAuth())
            $this->redirect('/user');
        IS::app()->session()->removeContainer('newPassword');
        $errorData = false;
        if(isset($_GET['token'])) {
            $tokenArray = explode(':', $_GET['token']);
            $user = new UsersModel(['rememberToken' => $_GET['token']]);
            if(count($tokenArray) < 2 || ($tokenArray[1]+(3600*12)) < time() || $user->insert ) {
                $errorData = 'Link expired. Try again';
            }else{
                IS::app()->session()->container('newPassword')->uid($user->getId());
            }
        }
        if(IS::app()->request()->isPost())
        {
            if(IS::app()->request()->postData('password') && (int)IS::app()->session()->container('newPassword')->uid()) {
                $id = (int)IS::app()->session()->container('newPassword')->uid();
                $user = new UsersModel($id);
                $user->setPassword(SignUpModel::cryptPassword(IS::app()->request()->postData('password')))
                    ->setRememberToken(0)
                    ->save();
                IS::app()->session()->setFlash('success', 'Password success changed');
                IS::app()->session()->removeContainer('newPassword');

                $this->redirect('/user/login');
            }
            $user = new UsersModel(['email' => IS::app()->request()->postData('email')]);
            if($user->insert){
                IS::app()->session()->setFlash('reset-error', 'User not exists with this email');
                $this->redirect('/user/reset');
            }else{
                $token = base64_encode(md5(Random::randomPassword(8))).':'.time();
                $user->setRememberToken($token)->save();
                Mailer::sendMessage($user->getEmail(), 'Password reset: ', Mailer::prepare('password_reset', [
                    '{LINK}' => 'http://'.$_SERVER['HTTP_HOST'].'/user/reset?token='.$token,
                ]));
                IS::app()->session()->setFlash('reset-ok', 'Check you email');
            }
        }
        IS::app()->set('title', 'Reset password', 1);
        return new ViewBase([
            'error' => $errorData,
        ]);
    }
}