<?php
namespace Controller;
use isv\Controller\ControllerBase;
use isv\IS;
use isv\View\ViewBase;
/**
 * ISV framework
 * Class IndexController
 * @package Controller
 */
class IndexController extends ControllerBase
{
    public function indexAction()
    {
        IS::app()->set('title', IS::app()->settings('frontend')->siteName->value);
        return new ViewBase();
    }
}