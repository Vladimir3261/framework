<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 12/4/16
 * Time: 4:34 PM
 */

return [
    'notify' => [
        'smtp' => false,
        'smtp_host' => '',
        'smtp_username' => '',
        'smtp_password' => '',
        'smtp_port' => 465,
        'secure_type' => 'ssl',
        'senderName' => 'Admin',
        'senderEmail' => '',
    ],
];
