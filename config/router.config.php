<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 04.02.16
 * Time: 19:53
 */
return [
    'router' => [
        /********************* ADMIN ROUTES ********************/
        'admin/portfolio/{action}/{id}' => 'Admin\PortfolioController',
        'admin/articles/{action}/{id}' => 'Admin\ArticlesController',
        'admin/index/{action}/{id}' => 'Admin\IndexController',
        'admin/settings/{action}/{id}' => 'Admin\SettingsController',
        'admin' => 'Admin\IndexController',
        /********************* USER ROUTES ********************/
        'isv/{action}/{id}/{name}'   => 'IsvController',
        'user/{action}/{id}'           => 'UserController',
    ]
];