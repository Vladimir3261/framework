<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 30.03.16
 * Time: 17:56
 */
return [
    'db' => [
        'MYSQL' => [
            'host'     => '127.0.0.1',
            'driver'   => 'pdo_mysql',
            'user'     => 'root',
            'password' => '123456',
            'dbname'   => 'isv',
            'charset'  => 'utf8',
        ],
    ]
];
