<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 11.11.16
 * Time: 16:28
 */

namespace Component;


use isv\Component\ISVComponent;

class TestComponent extends ISVComponent
{
    public function init()
    {
        // TODO: Implement init() method.
    }

    public function test($name, $price)
    {
       return ['name' => $name, 'price' => $price];
    }
}