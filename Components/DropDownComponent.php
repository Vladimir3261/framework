<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 30.04.17
 * Time: 1:08
 */
namespace Component;
use isv\Component\ISVComponent;
use isv\Component\ISVComponentInterface;
use isv\DB\ModelBase;
/**
 * Class DropDownComponent
 * @package Component
 */
class DropDownComponent extends ISVComponent implements ISVComponentInterface
{
    protected $params;

    public function init()
    {
        // TODO: Implement init() method.
    }

    /**
     * Get DropDown data array from Model
     * @param $params
     * @return array|mixed
     */
    public function getArray($params)
    {
        $this->params = $params;
        if(!isset($this->params['key']) || !isset($this->params['value']) || !isset($this->params['class']))
            return [];
        /**
         * @var $model ModelBase
         */
        $result = [];
        if(isset($this->params['begin']) && count($this->params['begin'])){
            foreach ($this->params['begin'] as $k => $v)
                $result[$k] = $v;
        }
        $model = new $this->params['class'];
        $model->SetPdoFetchMode(\PDO::FETCH_KEY_PAIR);
        $data = $model->findAll($this->params['key'].', '.$this->params['value']);
        if(count($result)) {
            foreach ($data as $k => $v)
                $result[$k] = $v;
        }else{
            $result = $data;
        }
        if(isset($this->params['end']) && count($this->params['end'])){
            foreach ($this->params['end'] as $k => $v)
                $result[$k] = $v;
        }
        return $result;
    }
}