<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 6/4/17
 * Time: 9:31 PM
 */
namespace Component;
use isv\Component\ISVComponent;
use isv\IS;
/**
 * Using file system.
 * Class FsComponent
 * @package Component
 */
class FsComponent extends ISVComponent
{
    private $public;
    private $files;

    const DIRECTORY = 1;
    const FILE = 2;

    public function init()
    {
        $this->public = ROOTDIR.DIRSEP.IS::app()->getConfig('config')['publicDir'];
        $this->files = $this->public.DIRSEP.'files';
    }

    public function scanDir($directory)
    {
        if(!is_dir($directory))
            return [];
        $raw = scandir($directory);
        $data = [];
        if($raw && count($raw))
        {
            foreach ($raw as $item)
            {
                if($item !== '.' && $item !== '..')
                {
                    if(is_dir($directory.DIRSEP.$item))
                    {
                        $data[$item] = [
                            'type' => static::DIRECTORY,
                            'absolutePath' => $directory,
                            'webPath' => str_replace($this->getPublicDir(), '', $directory),
                            'filesCount' => (count(scandir($directory.DIRSEP.$item)) -2)
                        ];
                    }
                    if(is_file($directory.DIRSEP.$item))
                    {
                        $data[$item] = [
                            'type' => static::FILE,
                            'absolutePath' => $directory,
                            'webPath' => str_replace($this->getPublicDir(), '', $directory).DIRSEP.$item,
                            'pathInfo' => pathinfo($directory.DIRSEP.$item),
                        ];
                    }
                }
            }
        }
        return $data;
    }

    public function getFilesDir($dir=null)
    {
        $path = $this->files;
        if($dir)
            $path .= DIRSEP.$dir;
        return str_replace('//', '/', $path);
    }

    public function getPublicDir($dir=null)
    {
        $path = $this->public;
        if($dir)
            $path .= DIRSEP.$dir;
        return str_replace('//', '/', $path);
    }

    public function remove($absolutePath)
    {
        if(is_file($absolutePath))
        {
            try {
                return @unlink($absolutePath);
            }catch (\Exception $e) {
                return ['error' => $e->getMessage()];
            }
        }else{
            return ['error' => 'file not exists'];
        }
    }

    public function upload($globalName, $dir)
    {
        if(!is_dir($dir))
            mkdir($dir, 0777);
        if(isset($_FILES[$globalName]) && $_FILES[$globalName]);
        $file = $_FILES[$globalName];
        $info = pathinfo($file['name']);
        $ext = $info['extension'];
        $name = 'upload_'.md5(time()).rand(1000, 1000);
        $fullName = $dir.DIRSEP.$name.'.'.$ext;
        return move_uploaded_file($file['tmp_name'], $fullName);
    }
}