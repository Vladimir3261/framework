<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 12/7/16
 * Time: 9:00 PM
 */
namespace Component;
use isv\Component\ISVComponent;
use isv\Component\ISVComponentInterface;

/**
 * Example for component
 * Class ConverterComponent
 * @package Component
 */
class ConverterComponent extends ISVComponent implements ISVComponentInterface
{
    public function init()
    {
        // Initialization
    }

    /**
     * Simple transliteration function
     * @param $word
     * @return string
     */
    public static function translit($word)
    {
        $replace = array(
            "А"=>"A","а"=>"a",
            "Б"=>"B","б"=>"b",
            "В"=>"V","в"=>"v",
            "Г"=>"G","г"=>"g",
            "Д"=>"D","д"=>"d",
            "Е"=>"Ye","е"=>"e",
            "Ё"=>"Ye","ё"=>"e",
            "Ж"=>"Zh","ж"=>"zh",
            "З"=>"Z","з"=>"z",
            "И"=>"I","и"=>"i",
            "Й"=>"Y","й"=>"y",
            "К"=>"K","к"=>"k",
            "Л"=>"L","л"=>"l",
            "М"=>"M","м"=>"m",
            "Н"=>"N","н"=>"n",
            "О"=>"O","о"=>"o",
            "П"=>"P","п"=>"p",
            "Р"=>"R","р"=>"r",
            "С"=>"S","с"=>"s",
            "Т"=>"T","т"=>"t",
            "У"=>"U","у"=>"u",
            "Ф"=>"F","ф"=>"f",
            "Х"=>"Kh","х"=>"kh",
            "Ц"=>"Ts","ц"=>"ts",
            "Ч"=>"Ch","ч"=>"ch",
            "Ш"=>"Sh","ш"=>"sh",
            "Щ"=>"Shch","щ"=>"shch",
            "Ъ"=>"","ъ"=>"",
            "Ы"=>"Y","ы"=>"y",
            "Ь"=>"","ь"=>"",
            "Э"=>"E","э"=>"e",
            "Ю"=>"Yu","ю"=>"yu",
            "Я"=>"Ya","я"=>"ya",
            " " => "_",
            "\"" => "",
        );
        return strtolower(str_replace(array_keys($replace), array_values($replace), $word));
    }

    /**
     * Make string with comma to float type like (string)3,14 => (float)3.14
     * @param $str
     * @return float
     */
    function strToFloat($str)
    {
        return (float)str_replace(',', '.', $str);
    }

    /**
     * Get count of days from date
     * @param $from
     * @return int
     */
    public static function dayFromDate($from)
    {
        $timeDiff = abs(time() - $from);
        $numberDays = $timeDiff/86400;  // 86400 seconds in one day
        return intval($numberDays);
    }
}